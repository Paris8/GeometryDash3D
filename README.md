# Geometry Dash en 3D

Pour lancer rapidement le jeu avec un serveur python :

```bash
$ ./run.sh
```

## But

Le but est de rester le plus longtemps en vie et donc d'esquiver tout les obstacles.
Chaque seconde passé dans le jeu rapporte 1 point.

## Dépendances

-   Télécharger [ThreeJS](https://github.com/mrdoob/three.js) et placer le
    fichier [`.js`](https://unpkg.com/three@0.146.0/build/three.min.js) dans
    [`js/lib`](./js/lib/).
-   Télécharger [dat.gui](https://github.com/dataarts/dat.gui) et placer le
    fichier [`.js`](https://cdnjs.cloudflare.com/ajax/libs/dat-gui/0.7.9/dat.gui.min.js) dans
    [`js/lib`](./js/lib/).

-   Placer une image `player.png` dans le dossier [`assets/images`](./assets/images/).

## Sources

-   [Collision example](https://github.com/stemkoski/stemkoski.github.com/blob/master/Three.js/Collision-Detection.html)
