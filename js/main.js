import { runGame } from "./Game.js";

window.addEventListener("load", () => main());

/**
 * Load the menu
 */
const main = () => {
    document.body.style = "background-color: black;";

    let titleGame = document.createElement("h1");
    titleGame.textContent = "GeometryDash 3D";
    titleGame.style =
        "animation-name: animTitle; \
         animation-iteration-count: infinite; \
         animation-duration: 2s; \
         background: none; \
         color: white; \
         font-size: 400%;";
    document.body.appendChild(titleGame);

    let demoGame = document.createElement("button");
    demoGame.textContent = "Partie démo";
    demoGame.style =
        "margin: 10%; \
         display: block; \
         margin-left: auto; \
         margin-right: auto; \
         border: none; \
         background: none; \
         color: white; \
         font-size: 400%; \
         cursor: pointer;";
    document.body.appendChild(demoGame);

    let normalGame = document.createElement("button");
    normalGame.textContent = "Partie normale";
    normalGame.style =
        "margin: 10%; \
         display: block; \
         margin-left: auto; \
         margin-right: auto; \
         border: none; \
         background: none; \
         color: white; \
         font-size: 400%; \
         cursor: pointer;";
    document.body.appendChild(normalGame);

    const removeMenu = () => {
        document.body.removeChild(titleGame);
        document.body.removeChild(normalGame);
        document.body.removeChild(demoGame);
    };

    normalGame.addEventListener(
        "click",
        () => {
            removeMenu();
            runGame(false);
        },
        { once: true }
    );

    demoGame.addEventListener(
        "click",
        () => {
            removeMenu();
            runGame(true);
        },
        { once: true }
    );
};
