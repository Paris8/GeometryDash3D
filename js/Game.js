import { Env, TypeEntity } from "./Env.js";
import { Plane } from "./Plane.js";
import { Player } from "./Player.js";
import { Quality } from "./utils.js";

/**
 * Jump event for the demo
 */
export const jumpDemo = new CustomEvent("jumpKey", {
    detail: {
        code: "Space",
    },
});

/**
 * Run the game
 * @param {boolean} demo
 */
export const runGame = (demo) => {
    // THREE.js
    const env = new Env();
    document.body.appendChild(env.getDomElement());

    // World
    const plan = new Plane(
        window.innerWidth / 50,
        10,
        THREE.Color.NAMES.white,
        THREE.Color.NAMES.black
    );
    env.addToScene(plan);

    // Player
    const player = new Player(THREE.Color.NAMES.pink);
    env.addToScene(player, TypeEntity.player);

    if (demo) {
        addEventListener("jumpKey", (e) => player.controlUser(e.detail));
    } else {
        addEventListener("keypress", player.controlUser);
    }

    // Generate random map
    env.generateRandomMap(10);

    // GUI
    const gui = new dat.gui.GUI({ closeOnTop: true });
    const menu = Quality.buildGUI(env.quality);
    for (const quality in menu) {
        gui.add(menu, quality)
            .listen()
            .onFinishChange(() => {
                Quality.update(env.setQuality, menu, quality);
            });
    }

    let score = document.getElementById("score");
    score.style =
        "position: absolute; \
         top: 10px; \
         width: 100%; \
         display: block; \
         color: white; \
         font-size: xx-large;";

    /**
     * Run the game
     */
    const animate = () => {
        const delta = env.clock.getDelta();
        const ticks = Math.round(delta / (1 / 120));
        for (let i = 0; i < ticks; i++) {
            if (env.update(demo)) {
                let end = document.createElement("p");
                end.textContent = "Partie terminée !";
                end.style =
                    "margin: 0; \
                     position: absolute; \
                     top: 50%; \
                     left: 35%; \
                     font-size: 400%; \
                     color: white;";
                document.body.appendChild(end);

                let restart = document.createElement("button");
                restart.textContent = "Cliquez ici pour redémarrer";
                restart.style =
                    "position: absolute; \
                     top: 60%; \
                     left: 27%; \
                     border: none; \
                     background: none; \
                     color: white; \
                     font-size: 400%; \
                     cursor: pointer;";
                document.body.appendChild(restart);

                restart.addEventListener("click", () => {
                    location.href = location.href;
                });

                gui.destroy();
                return;
            }
        }

        score.textContent = `Score : ${Math.floor(env.clock.elapsedTime)}`;

        requestAnimationFrame(animate);
    };

    // Start music
    env.music.play();

    // Run it
    animate();
};
