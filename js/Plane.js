import { Element } from "./Element.js";
import { textureGradient } from "./utils.js";

export class Plane extends Element {
    constructor(width, height, color1, color2) {
        super();

        const Tcolor1 = new THREE.Color(color1);
        const Tcolor2 = new THREE.Color(color2);

        this.data = new THREE.Mesh(
            new THREE.PlaneGeometry(width, height),

            new THREE.MeshPhongMaterial({
                color: color1,
                side: THREE.DoubleSide,
                map: textureGradient(
                    width,
                    height,
                    Tcolor1.getStyle(),
                    Tcolor2.getStyle()
                ),
            })
        );

        this.data.rotation.x = this.gameRotation;

        // Shadows
        this.data.receiveShadow = true;
    }
}
