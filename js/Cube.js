import { Element } from "./Element.js";

export class Cube extends Element {
    constructor(color, img) {
        super();

        let texture = null;
        if (img) {
            const loader = new THREE.TextureLoader();
            texture = loader.load(img);
        }

        this.data = new THREE.Mesh(
            new THREE.BoxGeometry(),
            new THREE.MeshPhongMaterial({ color: color, map: texture })
        );

        // Create shadows
        this.data.castShadow = true;

        // Move up
        this.data.position.y = 0.5;
    }
}
