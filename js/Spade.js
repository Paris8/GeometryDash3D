import { Cone } from "./Cone.js";

export class Spade extends Cone {
    constructor(color, level, startPos) {
        super(color, level, startPos);

        this.startPos = this.data.position.x + startPos;

        this.data.position.x = this.startPos;
    }
}
