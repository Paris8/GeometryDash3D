import { Element } from "./Element.js";

export class Size {
    static little = { radius: 0.5, height: 0.5, radialSegments: 5 };
    static big = { radius: 0.5, height: 0.8, radialSegments: 12 };
}

export class Cone extends Element {
    constructor(color, level) {
        super();

        this.data = new THREE.Mesh(
            new THREE.ConeGeometry(
                level.radius,
                level.height,
                level.radialSegments
            ),
            new THREE.MeshPhongMaterial({ color: color })
        );

        // Create shadows
        this.data.castShadow = true;

        // Move up
        this.data.position.y = 0.2;
    }
}
