export const textureGradient = (width, height, color1, color2) => {
    const cnv = document.createElement("canvas");

    const ctx = cnv.getContext("2d");
    ctx.rect(0, 0, cnv.width, cnv.height);

    const gradient = ctx.createLinearGradient(0, 0, width / 4, 15 * height);
    gradient.addColorStop(0, color1);
    gradient.addColorStop(1, color2);

    ctx.fillStyle = gradient;
    ctx.fill();

    const texture = new THREE.Texture(cnv);
    texture.needsUpdate = true;

    return texture;
};

export class Rotation {
    constructor(position) {
        this.default = position.clone();
        this.state = false;
        this.rotation = 0;
        this.jump = 0;

        this.rotationVelocity = 0.03;
        this.jumpVelocity = 0.06;
    }

    /**
     * Change the state of the animation
     */
    changeState = () => {
        this.state = !this.state;
    };

    /**
     * Change the final rotation
     * @param {number} rotation
     */
    changeRotation = (rotation) => {
        this.rotation = rotation;
    };

    /**
     * Change the final position
     * @param {number} jump
     */
    changeJump = (jump) => {
        this.jump = jump;
    };

    /**
     * Return the state of the jump
     * @returns boolean representing if the object is going up or down
     */
    falling = () => {
        return this.jump == this.default.y;
    };
}

export class Quality {
    static ultra = 1;
    static medium = 2;
    static low = 3;

    /**
     * Build an object of the settings panel
     * @param {Quality} defaultValue Default quality
     * @returns An object for dat.gui
     */
    static buildGUI = (defaultValue) => {
        const res = {
            Ultra: false,
            Medium: false,
            Low: false,
        };

        // Update the default value to true
        switch (defaultValue) {
            case this.ultra:
                res.Ultra = true;
                break;
            case this.medium:
                res.Medium = true;
                break;
            case this.low:
                res.Low = true;
                break;
        }

        // Return the object
        return res;
    };

    /**
     * Update the quality of the renderer
     * @param {property} fnQuality
     * @param {{string: boolean}} menu
     * @param {string} quality
     */
    static update = (fnQuality, menu, quality) => {
        // All the field to false
        for (const field in menu) {
            menu[field] = false;
        }

        // The select field to true
        menu[quality] = true;

        // Update the quality
        switch (quality) {
            case "Ultra":
                fnQuality(this.ultra);
                break;
            case "Medium":
                fnQuality(this.medium);
                break;
            case "Low":
                fnQuality(this.low);
                break;
        }
    };
}
