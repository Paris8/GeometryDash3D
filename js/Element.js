export class Element {
    constructor() {
        this.data = undefined;

        // Rotate everything on X by this factor to give some perspective
        this.gameRotation = 2;
    }

    /**
     * Animation of the element
     */
    animation = () => {};

    /**
     * Rotate the element
     */
    rotate = () => {
        if (this.data) {
            this.data.rotation.x += 0.01;
            this.data.rotation.y += 0.01;
        }
    };
}
